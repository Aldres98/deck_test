import { combineReducers } from "redux";
import mapbox from "./mapbox";
import window from "./window";


export default combineReducers({
	mapbox,
	window,
});
