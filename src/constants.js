import wNumb from "wnumb";

export const MAPBOX = {
	API_TOKEN: 'pk.eyJ1IjoiYWxkcmVzIiwiYSI6ImNqcjF2c2V0dTBnaDc0MnJwbmNjczM2ZTgifQ.VNyQzIrHBcpPlUcNbSpYyA',
  VIEWPORT: {
    width: '100%',
    height: '100vh',
    latitude: 56.056516,
    longitude: 38.263599,
    zoom: 4,
    mapStyle: 'mapbox://styles/mapbox/light-v9',
  }
}

