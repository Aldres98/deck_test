import React from "react";
import { connect } from "react-redux";
import DeckGL from '@deck.gl/react';
import $ from 'jquery';
import {COORDINATE_SYSTEM} from '@deck.gl/core';
import MapBox from "./components/MapBox/MapBox";
import { MAPBOX } from "./constants";
import { changeWindowSize } from './redux/actions';
import {LineLayer} from '@deck.gl/layers';
import {StaticMap} from 'react-map-gl';
import {HexagonLayer} from '@deck.gl/aggregation-layers';
import {ArcLayer} from '@deck.gl/layers';
import {ScatterplotLayer} from '@deck.gl/layers';
import {customData} from './data'
import {test} from './data'
import {GeoJsonLayer} from '@deck.gl/layers';
import "./styles.css";


const initialViewState = {
	longitude: -122.41669,
	latitude: 37.7853,
	zoom: 13,
	pitch: 0,
	bearing: 0
  };

  const data = [{sourcePosition: [37.602315, 55.72432], targetPosition: [-122.41669, 37.781]},{sourcePosition: [-122.41669, 37.7853], targetPosition: [-122.41669, 37.781]}, {sourcePosition: [-121.41669, 36.7853], targetPosition: [-120.41669, 35.781]}];



class App extends React.Component {
  	constructor(props) {
		  super(props);
		  
	
  		this.props = props;

  		this.changeWindowSizeState = props.changeWindowSize;
  	}

  	componentWillMount(){
  		const changeWindowSizeState = () => {
  			const width = $(window).width();
  			const height = $(window).height();
  			this.changeWindowSizeState(width, height);
  		}

  		changeWindowSizeState();

  		const _this = this;
  		$(window).on('resize', function(){
  			changeWindowSizeState();
  		});
	}

  	render() {
		  let dt = customData.features
		// const layer = new ArcLayer({
		// 	id: 'arc-layer',
		// 	data,
		// 	coordinateSystem: COORDINATE_SYSTEM.LNGLAT,
		// 	pickable: true,
		// 	getHeight: 5,
		// 	getWidth: 12,
		// 	getSourcePosition: d => d.sourcePosition,
		// 	getTargetPosition: d => d.targetPosition,
		// 	getSourceColor: d => [231, 140, 0],
		// 	getTargetColor: d => [31, 140, 0],
		// 	onHover: (info, event) => console.log('Hovered:', info, event),
		//   });

		const layer = new ScatterplotLayer({
			id: 'scatterplot-layer',
			dt,
			pickable: true,
			opacity: 0.8,
			stroked: true,
			filled: true,
			radiusScale: 6,
			radiusMinPixels: 6,
			radiusMaxPixels: 100,
			lineWidthMinPixels: 1,
			getPosition: d => d.geometry.coordinates,
			getRadius: d => Math.sqrt(d.exits),
			getFillColor: d => [255, 140, 0],
			getLineColor: d => [0, 0, 0],

		  });


		const layers = [
			layer
		  ];	  
  		return (
			<div className="railroads-app">
			      <DeckGL
        initialViewState={initialViewState}
        controller={true}
        layers={layers}
      >
					  <StaticMap mapboxApiAccessToken={MAPBOX.API_TOKEN} />

      </DeckGL>

			</div>
		);
  	}
}

export default connect(
  null,
  { changeWindowSize },
)(App);